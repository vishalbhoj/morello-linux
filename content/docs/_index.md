---
title: 'Overview'
date: 2023-04-25T15:14:39+10:00
weight: 1
---

## Morello Linux

In less than 10 minutes you should be able to setup a docker container with everything you need to build and boot into a Morello Debian environment.
